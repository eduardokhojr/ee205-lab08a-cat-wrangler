###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author @todo eduardo kho jr <eduardok@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   @todo 4/12/2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

all: main tmain queueSim 

main.o:  main.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

node.o: node.cpp node.hpp
	$(CXX) -c $(CXXFLAGS) $<

list.o: list.cpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

test.o: test.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

#queue.o: queue.hpp 
#	$(CXX) -c $(CXXFLAGS) $<

test: *.hpp test.o cat.o node.o list.o
	$(CXX) -o $@ test.o cat.o node.o list.o

main: *.hpp main.o cat.o node.o list.o
	$(CXX) -o $@ main.o cat.o node.o list.o

tmain: *.hpp tmain.o cat.o node.o list.o
	$(CXX) -o $@ tmain.o cat.o node.o list.o

queueSim.o: queueSim.cpp *.hpp
	$(CXX) -c $(CXXFLAGS) $<

queueSim: *.hpp queueSim.o list.o node.o 
	$(CXX) -o $@ queueSim.o list.o node.o 

tmain.o: tmain.cpp cat.hpp node.hpp
	$(CXX) -c $(CXXFLAGS) $<
clean:
	rm -f *.o main queueSim test tmain
