///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - cat wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// list for cat wrangler 
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 08a - cat wrangler - EE 205 - Spr 2021
/// @date   4/6/21
///////////////////////////////////////////////////////////////////////////////
#include"list.hpp"
#include"node.hpp"
#include<cassert>
#include<iostream>
using namespace std;
const bool DoubleLinkedList::empty() const{

	if (head==nullptr){
	return true;
		}else{
	return false;
		}
	return 0;
} 

void DoubleLinkedList::push_front(Node* newNode){ //changed//  

	if(newNode == nullptr){ 
		return;
	}
	if(head!=nullptr){
		newNode -> next = head;
		newNode -> prev = nullptr;
		head -> prev = newNode;
		head = newNode;
	}else{
		newNode -> next = nullptr;
		newNode -> prev = nullptr;
		head = newNode;
		tail = newNode;
	}
	count++;
}
Node* DoubleLinkedList::pop_front(){
	
	if(head == nullptr){
	return nullptr;
	}
	if(head == tail){
	return nullptr;
	}

	//sll
	//Node* temp;
	//temp = head;
	//head = head -> next;
	//return temp;
	//
	
	//dll
	Node* temp;
	temp = head;
	head = head -> next;
	head -> prev = nullptr;
	return temp;
}

Node* DoubleLinkedList::get_first() const{
	if(head == nullptr){
	return nullptr;
	}
	return head;
}

Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
	return currentNode->next;
}

/*inline unsigned int DoubleLinkedList::size() const{
	int count = 0;
	Node* current = head;
	while ( current != 0 ){
		count++;
		current = current -> next;
	}	
	return count;
}*/

void DoubleLinkedList::push_back(Node* newNode){
	if(newNode == nullptr){
		return;	
	}
	//not empty
	if(tail != nullptr){
		newNode->next = nullptr;
		newNode->prev = tail;
		tail->next = newNode;
		tail = newNode;
	}else{ //is empty
		newNode->next = nullptr;
		newNode->prev = nullptr;
		head = newNode;
		tail = newNode;
	}
	count++;
}

Node* DoubleLinkedList::pop_back(){
	/*Node* temp;
	if(tail == nullptr){
	return nullptr;
	}
	if(head == tail){
	return nullptr;
	}
	temp = tail;
	//temp -> prev = nullptr;
	tail = tail -> prev;
	tail -> next = nullptr;
	return temp;
	*/
	Node* temp;
	if(tail == nullptr){
	return nullptr;
	}
	if(head == tail){
	

	return head;
	}	
	
	temp = tail;
	tail = tail -> prev;
	tail -> next = nullptr;
	return temp;
}


Node* DoubleLinkedList::get_last() const{
	if(tail == nullptr){
	return nullptr;
	}
	return tail;


}
Node* DoubleLinkedList::get_prev( const Node* currentNode ) const{
	return currentNode->prev;
}

/*Node* DoubleLinkedList::insert_after( Node* currentNode, Node* newNode ){
	if(currentNode == nullptr){
		return nullptr;
	}*/
//	newNode->next = currentNode->next;
//	currentNode->next = newNode;
//	newNode->prev = currentNode;
//	if(newNode->next != nullptr){
//		newNode->next->prev = newNode;
//	}
	
/*	if(currentNode == tail){	
		tail = tail -> prev
		newNode = tail;
		}
	if(head == nullptr){
		newNode = head;
		}
}
*/
/*Node* DoubleLinkedList::insert_before( Node* currentNode, Node* newNode ){
	if(


}
*/

void DoubleLinkedList::swap( Node* x, Node* y ) {
	Node* yprev = y -> prev;
	Node* ynext = y -> next;
	Node* xnext = x -> next;
	Node* xprev = x -> prev;
/*	Node* temp; 
	temp = node1->next;
  	node1->next = node2->next;
  	node2->next = temp;
  	if (node1->next != nullptr){
    	node1->next->prev = node1;
	}
  	if (node2->next != nullptr){
    		node2->next->prev = node2;
 	}
	temp = node1->prev;
	node1->prev = node2->prev;
  	node2->prev = temp;
	if (node1->prev != nullptr){
    		node1->prev->next = node1;
	}
  	if (node2->prev == nullptr){
    		return;
	}
  	node2->prev->next = node2;
  		return;
*/
//case 1
if (x == head && y == tail){
	if( x->next != y ){
		head = y;
		tail = x;
		y -> prev -> next = x;
		x -> next -> prev = y;
		x -> prev = yprev;
		y -> prev = nullptr;
		y -> next = xnext;
		x -> next = nullptr;
	}else{}
	return;
}
//case 2
if (x == head && y != tail){
	if( x->next != y){
		head = y;
		y -> prev -> next = x;
		x -> next -> prev = y;
		x -> prev = yprev;
		y -> prev = nullptr;
		y -> next = xnext;
		x -> next = ynext;
	}else{ //adjacent
		head = y;
		x -> prev = y;
		y -> prev = nullptr;
 		y -> next = x;
		x -> next = ynext;
	}
	return;
}

//case 3
if (x != head && y == tail){
	if( x->next != y){
		tail = x;
		x -> prev -> next = y;
		y -> next -> prev = x;
		x -> prev = yprev;
		y -> prev = xprev;
		x -> next = nullptr;
		y -> next = xnext;
	}else{
		tail = x;
		x -> prev = y;
		y -> prev = xprev;
		x -> next = nullptr;
		y -> next = xnext;
	}
}

//case 4
if (x != head && y != tail){
	if( x->next != y){
		y -> prev -> next = x;
		x -> prev -> next = y;
		x -> prev = yprev;
		x -> next = ynext;
		y -> next = xnext;
		y -> prev = xprev;
	}else{
		y -> prev = xprev;
		y -> next = x;
		x -> prev = y;
		x -> next = ynext;
	}

}
}


const bool DoubleLinkedList::isSorted() const{
	if(count <= 1){
		return true;
	}
	for( Node* i = head; i->next !=nullptr; i = i->next){
		if(*i > *i->next){
			return false;
	}
	}
	return true;
}

void DoubleLinkedList::insertionSort() {
	
	if( count <= 1 ){
		return;
	}
	for( Node* i = head; i->next != nullptr; i = i->next){
		Node* minNode = i;
		for( Node* j = i->next; j != nullptr; j = j->next){
			if(*minNode > *j){
				minNode = j;
			}
		}
		swap(i, minNode);
		i = minNode;
	}

}
/*
bool DoubleLinkedList::validate() const {
	if(head == nullptr){
		assert( tail == nullptr );
		assert( count == 0 );
	}else{
		assert( tail != nullptr );
		assert( count != 0 );
	}
	
	if( tail == nullptr ) {
		assert( head == nullptr );
		assert( count == 0 );
	}else{
		assert( head!= nullptr );
		assert( count!= 0 );
	}
		
	if( head != nullptr && tail == head ) {
		assert( count == 1 );
	}
	
	unsigned int forwardCount = 0;
	Node* currentNode = head;
	while( currentNode != nullptr ){
		forwardCount++;
		if( currentNode->next != nullptr ){
			assert( currentNode->next->prev == currentNode );
		}
		currentNode = currentNode->next;
	}
	assert( count == forwardCount );
	//back
	unsigned int backwardCount = 0;
	currentNode = tail;
	while( currentNode != nullptr ){
		backwardCount++;
		if( currentNode->prev != nullptr ){
			assert( currentNode->prev->next == currentNode);
		}
		currentNode = currentNode->prev;
	}
	assert(count == backwardCount);

	return true;
} */
