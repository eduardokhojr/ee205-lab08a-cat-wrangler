///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - cat wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// list header for cat wrangler 
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 08a - cat wrangler - EE 205 - Spr 2021
/// @date   4/6/21
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include"node.hpp"

class DoubleLinkedList{
	public:
		bool validate() const;
		int count = 0;
		const bool isSorted() const; // this func depends on node's < operator
		void insertionSort(); // this func depends on node's < operator
		void swap( Node* node1, Node* node2 ); // generic swap.
		const bool empty() const; // return true if the list is empty. false if there is anything in it
		void push_front ( Node* newNode ) ; // add newNode to the front of the list
		Node* pop_front(); // remove a node from the front of the list. if the list is already empty, return nullptr
		Node* get_first() const; // return the very first node from the list. Dont make any changes to the list.
		Node* get_next( const Node* currentNode ) const; // return the node immediately following currentNode.
		inline unsigned int size() const{
			int c = 0;
			Node* current = head;
			while ( current !=0 ) {
				c++;
				current = current -> next;
				}
			return c;		
				
		} // return the number of nodes in the list.
		void push_back( Node* newNode ); // add newNode to the back of the list.
		Node* pop_back(); // Remove a node from the back of the list. if the list is already empty, return null ptr.
		Node* get_last() const; // return the very last node from the list. don't make any changees to the list.
		Node* get_prev( const Node* currentNode) const; // return the node immediately before currentNode.
		//void insert_after( Node* currentNode, Node* newNode); // insert newNode immediately after currentNode. Note: this could have been done with just a singly linked list.
		//void insert_before( Node* currentNode, Node* newNode ); // insert newNode immediately before currentNode.			
		Node* next = nullptr;
		Node* prev = nullptr;
	protected:
		Node* head = nullptr;	
		Node* tail = nullptr;
};



