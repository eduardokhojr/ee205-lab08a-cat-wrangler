///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file tmain.cpp
/// @version 1.0
///
/// test main 
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   4/6/2021
/////////////////////////////////////////////////////////////////////////////////

#include<iostream>
#include"cat.hpp"
#include"node.hpp"
#include"list.hpp"
using namespace std;

int main(){

DoubleLinkedList list = DoubleLinkedList();
for( int i = 0 ; i < 8 ; i++ ) {
	list.push_front(Cat::makeCat());
	cout << "gay" << endl;
	list.push_back(Cat::makeCat());
} 

for ( Cat* cat = (Cat*)list.get_first() ; cat != nullptr ; cat = (Cat*)list.get_next( cat )) {
	cout << cat->getInfo() << endl;
}

Cat* firstcat = (Cat*)list.get_first();
Cat* lastcat = (Cat*)list.get_last();
Cat* beforecat = (Cat*)list.get_prev(list.get_last());
Cat* nextcat = (Cat*)list.get_next(list.get_first());

cout << "watch thses fakas" << endl;
cout << firstcat->getInfo() << endl;
cout << nextcat->getInfo() << endl;
cout << beforecat->getInfo() << endl;
cout << lastcat->getInfo() << endl;
/*
cout << "head and tail" << endl;
list.swap( list.get_first(), list.get_last() );
for( Cat* cat = (Cat*)list.get_first() ; cat != nullptr; cat = (Cat*)list.get_next(cat)) {
	cout<< cat->getInfo() << endl;
}
*/
cout << "head and not tail" << endl;
list.swap( list.get_first(), list.get_prev(list.get_last()) );
for ( Cat* cat = (Cat*)list.get_first() ; cat != nullptr; cat = (Cat*)list.get_next( cat)) {
	cout<<cat->getInfo()<<endl;
}
/*
cout << "not head and tail" << endl;
list.swap(list.get_next(list.get_first()), list.get_last() );
for( Cat* cat = (Cat*)list.get_first(); cat!=nullptr; cat = (Cat*)list.get_next(cat)){
	cout<<cat->getInfo()<<endl;
}

cout << "not head not tail" <<endl;
list.swap( list.get_next(list.get_first()), list.get_prev(list.get_last()));
for( Cat* cat = (Cat*)list.get_first(); cat!=nullptr; cat = (Cat*)list.get_next(cat)){
	cout<<cat->getInfo() << endl;
}
*/
}









